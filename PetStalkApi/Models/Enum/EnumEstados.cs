﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.Enum
{
    public enum EnumEstados
    {
        cero = 0,
        Activo = 1,
        Inactivo = 2,
        Bloqueado = 3,
        Eliminado = 4,

        No_validado = 1,
        Validado = 2
    }
}