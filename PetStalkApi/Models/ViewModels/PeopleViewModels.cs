﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.ViewModels
{
    public class PeopleViewModels
    {
        public int? id_users { get; set; }
        public int? id_perfil { get; set; }
        public int? id_status { get; set; }
        public string status { get; set; }
        public string nombre { get; set; }
        public string username { get; set; }
        public string token { get; set; }
    }
}