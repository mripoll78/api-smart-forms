﻿using PetStalkApi.Models;
using PetStalkApi.Models.Enum;
using PetStalkApi.Models.Jwt;
using PetStalkApi.Models.ViewModels;
using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;

namespace PetStalkApi.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("API/LOGIN")]
    public class LoginController : ApiController
    {
        PetStalkApi.Models.ReCaptcha.GoogleReCaptcha ctrlGoogleReCaptcha = new PetStalkApi.Models.ReCaptcha.GoogleReCaptcha();

        private FormsIntelligentEntities db = new FormsIntelligentEntities();

        public struct SedToken
        {
            public PeopleViewModels token;
            public SedToken(PeopleViewModels toke)
            {
                token = toke;
            }
        }

        public struct SedTipoMensaje
        {
            public int Id;
            public string Descripcion;
            public string Tipo;
            public SedTipoMensaje(int toke, string nombr, string ape)
            {
                Id = toke;
                Descripcion = nombr;
                Tipo = ape;
            }
        }

        [HttpPost]
        [Route("AUTHENTICATE")]
        public IHttpActionResult Authenticate(LoginRequest login)
        {
            try
            {
                ctrlGoogleReCaptcha.PublicKey = ConfigurationManager.AppSettings["ReCaptchaPublicKey"].ToString();
                ctrlGoogleReCaptcha.PrivateKey = ConfigurationManager.AppSettings["ReCaptchaPrivateKey"].ToString();

                //if (ctrlGoogleReCaptcha.Validate(login.token))
                //{
                Users objUserAdmin = db.Users.FirstOrDefault(x => x.username == login.Username);

                if (objUserAdmin != null)
                {
                    bool Valid = Cipher.Decrypt(objUserAdmin.password).Equals(login.Password);
                    Users ValidarPerfil = db.Users.FirstOrDefault(x => x.username == login.Username && Valid == true);
                    Users ValidarUser = db.Users.FirstOrDefault(x => x.username == login.Username && x.numero_intento <= 5);

                    if (ValidarPerfil != null)
                    {
                        if (ValidarPerfil.eliminado == false)
                        {
                            if (ValidarPerfil != null && ValidarUser != null)
                            {
                                if (login == null) throw new HttpResponseException(HttpStatusCode.BadRequest);

                                MessageRequest msr = ValidarCredenciales(login.Username, login.Password, Convert.ToInt32(ValidarPerfil.id_profile));
                                bool isCredentialValid = msr.validation;
                                if (isCredentialValid)
                                {
                                    PeopleViewModels People = new PeopleViewModels();

                                    People BuscarPersona = db.People.FirstOrDefault(x => x.id == ValidarPerfil.id_people && Valid == true);
                                    Users Buscarperfil = db.Users.FirstOrDefault(x => x.id_people == BuscarPersona.id && Valid == true);


                                    People.id_users = Buscarperfil.id;
                                    People.id_perfil = Buscarperfil.id_profile;
                                    People.id_status = Buscarperfil.id_status;
                                    People.nombre = BuscarPersona.names + " " + BuscarPersona.surnames;
                                    People.username = Buscarperfil.username;
                                    People.status = "Success";
                                    People.token = TokenGenerator.GenerateTokenJwt(msr.entero.ToString());

                                    ValidarUser.numero_intento = 0;
                                    db.Entry(ValidarUser).State = EntityState.Modified;
                                    db.SaveChanges();

                                    SedToken To = new SedToken(People);

                                    return Ok(People);
                                }
                                else
                                {
                                    SedTipoMensaje To = new SedTipoMensaje((int)EnumTiposMensajes.UsuarioBloqueado, msr.answer, "Error");
                                    return Ok(To);
                                }
                            }
                            else
                            {
                                if (ValidarUser != null)
                                {
                                    ValidarUser.numero_intento = ValidarUser.numero_intento + 1;
                                    db.Entry(ValidarUser).State = EntityState.Modified;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    SedTipoMensaje Tos = new SedTipoMensaje((int)EnumTiposMensajes.NumeroIntentos, "Usuario bloqueado, intente recuperar su contraseña", "Error");
                                    return Ok(Tos);
                                }

                                SedTipoMensaje To = new SedTipoMensaje((int)EnumTiposMensajes.ContraseñaIncorrecta, "Contraseña incorrecta", "Error");
                                return Ok(To);
                            }
                        }
                        else
                        {
                            SedTipoMensaje To = new SedTipoMensaje((int)EnumTiposMensajes.UsuarioBloqueado, "Usuario bloqueado contacte a soporte", "Error");
                            return Ok(To);
                        }
                    }
                    else
                    {
                        SedTipoMensaje To = new SedTipoMensaje((int)EnumTiposMensajes.ContraseñaIncorrecta, "Contraseña incorrecta", "Error");
                        return Ok(To);
                    }
                }
                else
                {
                    SedTipoMensaje To = new SedTipoMensaje((int)EnumTiposMensajes.ContraseñaIncorrecta, "Usuario incorrecto", "Error");
                    return Ok(To);
                }
                //}
                //else
                //{
                //    SedTipoMensaje To = new SedTipoMensaje((int)EnumTiposMensajes.tokenexpi, "Error Captcha", "Error");
                //    return Ok(To);
                //}
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        private MessageRequest ValidarCredenciales(string username, string password, int perfil)
        {
            MessageRequest msr = new MessageRequest();
            if (perfil == (int)EnumPerfiles.Administrador)
            {
                Users objUserAdmin = db.Users.FirstOrDefault(x => x.username == username);
                if (objUserAdmin != null)
                {
                    if (objUserAdmin.eliminado == false)
                    {
                        if (Cipher.Decrypt(objUserAdmin.password).Equals(password))
                        {
                            msr.answer = "";
                            msr.validation = true;
                            msr.entero = objUserAdmin.id;
                            msr.user = 1;
                        }
                        else
                        {
                            msr.answer = "Contraseña incorrecta";
                            msr.validation = false;
                        }
                    }
                    else
                    {
                        msr.answer = "Usuario no verificado";
                        msr.validation = false;
                    }
                }
                else
                {
                    msr.answer = "Usuario incorrecto";
                    msr.validation = false;
                }
            }
            else
            {
                Users objUsuario = db.Users.FirstOrDefault(x => x.username == username);
                if (objUsuario != null)
                {
                    if (objUsuario.eliminado == false)
                    {
                        if (Cipher.Decrypt(objUsuario.password).Equals(password))
                        {
                            msr.answer = "";
                            msr.validation = true;
                            msr.entero = objUsuario.id;
                            msr.user = 2;
                        }
                        else
                        {
                            msr.answer = "Contraseña incorrecta";
                            msr.validation = false;
                        }
                    }
                    else
                    {
                        msr.answer = "Usuario no verificado";
                        msr.validation = false;
                    }
                }
                else
                {
                    msr.answer = "Usuario incorrecto";
                    msr.validation = false;
                }
            }
            return msr;
        }
        public class Encrypt
        {
            public static string GetMD5(string str)
            {
                MD5 md5 = MD5CryptoServiceProvider.Create();
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] stream = null;
                StringBuilder sb = new StringBuilder();
                stream = md5.ComputeHash(encoding.GetBytes(str));
                for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
                return sb.ToString();
            }
        }
    }
}