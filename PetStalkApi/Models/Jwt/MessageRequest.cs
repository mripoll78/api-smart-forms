﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.Jwt
{
    public class MessageRequest
    {
        public string answer { get; set; }
        public bool validation { get; set; }
        public int entero { get; set; }
        public int user { get; set; }
    }
}