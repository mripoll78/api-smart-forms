﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.ViewModels
{
    public class GetDataPeopleViewModels
    {
        public List<ProfileViewModels> ListProfile { get; set; }
        public List<GenderViewModels> ListGender { get; set; }
        public List<CityViewModels> ListCity { get; set; }
    }
}