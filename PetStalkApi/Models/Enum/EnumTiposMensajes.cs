﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.Enum
{
    public enum EnumTiposMensajes
    {
        //Status
        Success = 1,
        Warning = 2,
        Error = 3,

        //Login
        UsuarioIncorrecto = 1,
        ContraseñaIncorrecta = 2,
        TokenExpieradoCaptcha = 3,
        NumeroIntentos = 4,
        UsuarioBloqueado = 5,

        //EnviarCorreo
        CorreoEnviado = 1,
        CorreoNoExiste = 2,
        TokemExpiradoCorreo = 3,

        //Register
        UsuarioRegistrado = 1,
        IdentificacionExiste = 2,
        CorreoExiste = 3,
        UsuarioExiste = 4,

        //Package
        PaqueteCreado = 1,
        PaqueteNoCreado = 2,
        NoExistePaquete = 3,

        //Tickets
        TocketsCreado = 1,
        TicketsNoCreado = 2,

        //Wallet
        WalletAgregada = 1,
        WalleNoAgregada = 2,
        CodigoExpiradoWallet = 3,

        //User
        UserActualizado = 1,
        UsernoActualizado = 2,
        CodigoExpiradoUser = 3,
        PersonaNoExiste = 4,
        UsuarioNoExiste = 5,

        //verifi email
        No_verificacdo = 1,
        Verificado = 2,
        CodigoExpirado = 3,
        SuccessEnviado = 2,
        SuccessVerificado = 1,

        // CapCha
        tokenexpi = 7
    }
}