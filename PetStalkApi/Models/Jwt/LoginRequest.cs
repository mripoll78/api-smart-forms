﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.Jwt
{
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int perfil { get; set; }
        public string token { get; set; }
    }
}