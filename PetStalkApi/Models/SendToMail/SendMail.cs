﻿using PetStalkApi.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.SendToMail
{
    public class SendMail
    {
        public bool SendCorreo(string Correo, string Usuario, string Password, string Asunto, string Descripcion, string Titulo, string Url, string Token, string code, string walle)
        {
            string stCuerpoHTML = "<!DOCTYPE html>";
            stCuerpoHTML += "<html lang='es'>";
            stCuerpoHTML += "<head>";
            stCuerpoHTML += "<meta charset='utf - 8'>";
            stCuerpoHTML += "<title>" + Titulo + "</title>";
            stCuerpoHTML += "</head>";
            stCuerpoHTML += "<body style='background - color: black '>";
            stCuerpoHTML += "<table style='max - width: 600px; padding: 10px; margin: 0 auto; border - collapse: collapse; '>";
            stCuerpoHTML += "<tr>";
            stCuerpoHTML += "<td style='padding: 0'>";
            stCuerpoHTML += "<img style='padding: 0; margin: 0 auto; display: block' src='cid:Fondo' width='30%'>";
            stCuerpoHTML += "</td>";
            stCuerpoHTML += "</tr>";
            stCuerpoHTML += "<tr>";
            stCuerpoHTML += "<td style='background - color: #ecf0f1'>";
            stCuerpoHTML += "<div style='margin: 4% 10% 2%; text-align: justify;font-family: sans-serif'>";
            stCuerpoHTML += "<h2 style='margin: 0 0 7px'>Hola " + Usuario + "</h2>";
            stCuerpoHTML += "<p style='margin: 2px; font - size: 15px'>";
            stCuerpoHTML += Descripcion;
            if (Usuario != "" && Password != "")
            {
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "Usuario: " + Usuario + "";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "Contraseña: " + Password + "";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "<br/>";
            }
            else if (Url != "")
            {
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "Link de verificación: " + Url + Token + "";
            }
            else if (code != "")
            {
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "Codigo de verificacion: " + code + "";
            }
            else if (walle != "")
            {
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "<br/>";
                stCuerpoHTML += "Wallet: " + walle + "";
            }
            stCuerpoHTML += "<br/>";
            stCuerpoHTML += "<br/>";
            stCuerpoHTML += "<br/>";
            stCuerpoHTML += "<br/>";
            stCuerpoHTML += "Atentamente equipo NetWorkGolden.";
            stCuerpoHTML += "</p>";
            stCuerpoHTML += "<p style='color: #b3b3b3; font-size: 12px; text-align: center;margin: 30px 0 0'>Copyright © NetWorkGolden " + DateTime.Now.Year + "</p>";
            stCuerpoHTML += "</div>";
            stCuerpoHTML += "</td>";
            stCuerpoHTML += "</tr>";
            stCuerpoHTML += "</table>";
            stCuerpoHTML += "</body>";
            stCuerpoHTML += "</html>";

            clsCorreo obclsCorreo = new clsCorreo
            {
                stServidor = ConfigurationManager.AppSettings["stServidor"].ToString(),
                stUsuario = ConfigurationManager.AppSettings["stUsuario"].ToString(),
                stPassword = ConfigurationManager.AppSettings["stPassword"].ToString(),
                stPuerto = ConfigurationManager.AppSettings["stPuerto"].ToString(),
                blAutenticacion = true,
                blConexionSegura = true,
                inPrioridad = 0,
                inTipo = 1,
                stAsunto = Asunto,
                stFrom = ConfigurationManager.AppSettings["stUsuario"].ToString(),
                stTo = Correo,
                stImagen = AppDomain.CurrentDomain.BaseDirectory + "Adjunto\\Fondo.gif",
                stIdImagen = "Fondo",
                stMensaje = stCuerpoHTML
            };

            SendToMailController.setEmailController(obclsCorreo);

            return true;
        }
    }
}