﻿using PetStalkApi.Models;
using PetStalkApi.Models.Enum;
using PetStalkApi.Models.Jwt;
using PetStalkApi.Models.ViewModels;
using PetStalkApi.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Data;

namespace PetStalkApi.Controllers
{
    [Authorize]
    [RoutePrefix("API/PEOPLE")]
    public class PeopleController : ApiController
    {
        private FormsIntelligentEntities db = new FormsIntelligentEntities();

        public struct SedTipoMensaje
        {
            public List<ProfileViewModels> ListProfile;
            public List<TipoIdViewModels> ListTipoId;
            public List<GenderViewModels> ListGender;
            public List<CityViewModels> ListCity;
            public int Id;
            public string Type;
            public string Message;
            public SedTipoMensaje(List<ProfileViewModels> listProfile, List<TipoIdViewModels> lisTipoId, List<GenderViewModels> listGender, List<CityViewModels> listCity, int id, string type, string message)
            {
                ListProfile = listProfile;
                ListTipoId = lisTipoId;
                ListGender = listGender;
                ListCity = listCity;
                Id = id;
                Type = type;
                Message = message;
            }
        }
        public struct SedTipoMensaje2
        {
            public int Id;
            public string Type;
            public string Message;
            public SedTipoMensaje2(int id, string type, string message)
            {
                Id = id;
                Type = type;
                Message = message;
            }
        }
        public struct SedTipoMensaje3
        {
            public List<ProfileViewModels> ListProfile;
            public List<TipoIdViewModels> ListTipoId;
            public List<GenderViewModels> ListGender;
            public List<CityViewModels> ListCity;
            public ObjePersonasEntity ObjePerson;
            public int Id;
            public string Type;
            public string Message;
            public SedTipoMensaje3(List<ProfileViewModels> listProfile, List<TipoIdViewModels> lisTipoId, List<GenderViewModels> listGender, List<CityViewModels> listCity, ObjePersonasEntity objePerson, int id, string type, string message)
            {
                ListProfile = listProfile;
                ListTipoId = lisTipoId;
                ListGender = listGender;
                ListCity = listCity;
                ObjePerson = objePerson;
                Id = id;
                Type = type;
                Message = message;
            }
        }

        [HttpGet]
        [Route("GET_DATA_PEOPLE")]
        public IHttpActionResult GetDataPeople()
        {
            string IdUser = HttpContext.Current.User.Identity.Name;
            int IdUse = Convert.ToInt32(IdUser);

            List<ProfileViewModels> profile = new List<ProfileViewModels>();
            List<GenderViewModels> gender = new List<GenderViewModels>();
            List<CityViewModels> city = new List<CityViewModels>();
            List<TipoIdViewModels> type_id = new List<TipoIdViewModels>();

            try
            {               
                Users use = db.Users.FirstOrDefault(x => x.id == IdUse);
                if (use != null)
                {                   
                    foreach (var item in db.Profile)
                    {
                        ProfileViewModels profileViewModels = new ProfileViewModels();

                        profileViewModels.id = item.id;
                        profileViewModels.valor = item.valor;
                        profile.Add(profileViewModels);
                    }
                    foreach (var item in db.Type_id)
                    {
                        TipoIdViewModels tipoIdViewModels = new TipoIdViewModels();

                        tipoIdViewModels.id = item.id;
                        tipoIdViewModels.valor = item.valor;
                        type_id.Add(tipoIdViewModels);
                    }
                    foreach (var item in db.City)
                    {
                        GenderViewModels genderViewModels = new GenderViewModels();

                        genderViewModels.id = item.id;
                        genderViewModels.valor = item.valor;
                        gender.Add(genderViewModels);
                    }
                    foreach (var item in db.Gender)
                    {
                        CityViewModels cityViewModels = new CityViewModels();

                        cityViewModels.id = item.id;
                        cityViewModels.valor = item.valor;
                        city.Add(cityViewModels);
                    }

                    SedTipoMensaje To = new SedTipoMensaje(profile, type_id, gender, city, 1, "Success", "Datos crear persona");
                    return Ok(To);
                }
                else
                {
                    SedTipoMensaje To = new SedTipoMensaje(profile, type_id, gender, city, 1, "Invalid_Access", "Error");
                    return Ok(To);
                }
            }
            catch (Exception ex)
            {
                SedTipoMensaje To = new SedTipoMensaje(profile, type_id, gender, city, 2, "Exception", ex.Message);
                return Ok(To);
            }
        }

        [HttpPost]
        [Route("POST_CREATE_PEOPLE")]
        public IHttpActionResult PostCreatePeople(ObjCrearPersonas objCrearPersonas)
        {
            try
            {
                string IdUser = HttpContext.Current.User.Identity.Name;
                int IdUse = Convert.ToInt32(IdUser);

                Users use = db.Users.FirstOrDefault(x => x.id == IdUse);
                if (use != null && use.id_profile != 3)
                {
                    People requestPeopleId = db.People.Where(x => x.identifications == objCrearPersonas.identifications).FirstOrDefault();
                    People requestPeopleEmail = db.People.Where(x => x.email == objCrearPersonas.email).FirstOrDefault();
                    Users requestUsersExist = db.Users.Where(x => x.username == objCrearPersonas.usuario).FirstOrDefault();

                    if (requestPeopleId == null)
                    {
                        if (requestPeopleEmail == null)
                        {
                            if (requestUsersExist == null)
                            {
                                People people = new People();
                                people.id_type_id = objCrearPersonas.id_type_id;
                                people.id_city = objCrearPersonas.id_city;
                                people.identifications = objCrearPersonas.identifications;
                                people.names = objCrearPersonas.names;
                                people.surnames = objCrearPersonas.surnames;
                                people.email = objCrearPersonas.email;
                                people.phone = objCrearPersonas.phone;
                                people.eliminado = false;
                                people.id_gender = objCrearPersonas.id_gender;
                                people.fecha_sistema = Convert.ToDateTime(DateTime.Now.ToString());

                                db.People.Add(people);
                                try
                                {
                                    db.SaveChanges();

                                    Users users = new Users();
                                    users.id_people = people.id;
                                    users.id_profile = objCrearPersonas.id_profile;
                                    users.id_status = (int)EnumEstados.Activo;
                                    users.username = objCrearPersonas.usuario;
                                    users.password = Cipher.Encrypt(objCrearPersonas.password);
                                    users.numero_intento = (int)EnumEstados.cero;
                                    users.eliminado = false;
                                    users.fecha_sistema = Convert.ToDateTime(DateTime.Now.ToString());

                                    db.Users.Add(users);
                                    try
                                    {
                                        db.SaveChanges();
                                        SedTipoMensaje2 To = new SedTipoMensaje2(1, "Success", "Se creo correctamente la persona");
                                        return Ok(To);
                                    }
                                    catch (Exception ex)
                                    {
                                        SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                                        return Ok(To);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                                    return Ok(To);
                                }
                            }
                            else
                            {
                                SedTipoMensaje2 To = new SedTipoMensaje2(6, "Usuario existe", "Error");
                                return Ok(To);
                            }
                        }
                        else
                        {
                            SedTipoMensaje2 To = new SedTipoMensaje2(5, "Correo existe", "Error");
                            return Ok(To);
                        }
                    }
                    else
                    {
                        SedTipoMensaje2 To = new SedTipoMensaje2(4, "Identificacion existe", "Error");
                        return Ok(To);
                    }                    
                }
                else
                {
                    SedTipoMensaje2 To = new SedTipoMensaje2(2, "Invalid_Access", "Error");
                    return Ok(To);
                }
            }
            catch (Exception ex)
            {
                SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                return Ok(To);
            }
        }

        [HttpGet]
        [Route("GET_DATA_LIST_PEOPLE")]
        public IHttpActionResult GetDataListPeople()
        {
            string IdUser = HttpContext.Current.User.Identity.Name;
            int IdUse = Convert.ToInt32(IdUser);
            List<ObjePersonasEntity> ListObje = new List<ObjePersonasEntity>();
            try
            {
                Users use = db.Users.FirstOrDefault(x => x.id == IdUse);
                if (use != null && use.id_profile != 3)
                {
                    foreach (var item in db.Users.Where(x => x.id_profile != 1).ToList())
                    {
                        People people = db.People.Where(x => x.id == item.id_people).FirstOrDefault();
                        Status status = db.Status.Where(x => x.id == item.id_status).FirstOrDefault();
                        Profile profiles = db.Profile.Where(x => x.id == item.id_profile).FirstOrDefault();
                        Type_id type_Id = db.Type_id.Where(x => x.id == people.id_type_id).FirstOrDefault();
                        City citys = db.City.Where(x => x.id == people.id_city).FirstOrDefault();

                        ObjePersonasEntity objCrearPersonas = new ObjePersonasEntity();
                        objCrearPersonas.id = item.id;
                        objCrearPersonas.id_status = status.id;
                        objCrearPersonas.status = status.valor;
                        objCrearPersonas.identifications = people.identifications;
                        objCrearPersonas.profile = profiles.valor;
                        objCrearPersonas.type_id = type_Id.valor;
                        objCrearPersonas.city = citys.valor;
                        objCrearPersonas.names = people.names + " " + people.surnames;
                        objCrearPersonas.usuario = item.username;
                        objCrearPersonas.email = people.email;
                        objCrearPersonas.phone = people.phone;
                        objCrearPersonas.fecha_sistema = item.fecha_sistema;

                        ListObje.Add(objCrearPersonas);
                    }

                    return Ok(ListObje);
                }
                else
                {
                    SedTipoMensaje2 To = new SedTipoMensaje2(2, "Invalid_Access", "Error");
                    return Ok(To);
                }
            }
            catch (Exception ex)
            {
                SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                return Ok(To);
            }
        }

        [HttpGet]
        [Route("GET_DATA_PEOPLE_ID")]
        public IHttpActionResult GetDatatPeople_Id(int id)
        {
            string IdUser = HttpContext.Current.User.Identity.Name;
            int IdUse = Convert.ToInt32(IdUser);

            List<ProfileViewModels> profile = new List<ProfileViewModels>();
            List<GenderViewModels> gender = new List<GenderViewModels>();
            List<CityViewModels> city = new List<CityViewModels>();
            List<TipoIdViewModels> type_id = new List<TipoIdViewModels>();

            try
            {
                Users use = db.Users.FirstOrDefault(x => x.id == IdUse);
                if (use != null && use.id_profile != 3)
                {
                    foreach (var item in db.Profile)
                    {
                        ProfileViewModels profileViewModels = new ProfileViewModels();

                        profileViewModels.id = item.id;
                        profileViewModels.valor = item.valor;
                        profile.Add(profileViewModels);
                    }
                    foreach (var item in db.Type_id)
                    {
                        TipoIdViewModels tipoIdViewModels = new TipoIdViewModels();

                        tipoIdViewModels.id = item.id;
                        tipoIdViewModels.valor = item.valor;
                        type_id.Add(tipoIdViewModels);
                    }
                    foreach (var item in db.City)
                    {
                        GenderViewModels genderViewModels = new GenderViewModels();

                        genderViewModels.id = item.id;
                        genderViewModels.valor = item.valor;
                        gender.Add(genderViewModels);
                    }
                    foreach (var item in db.Gender)
                    {
                        CityViewModels cityViewModels = new CityViewModels();

                        cityViewModels.id = item.id;
                        cityViewModels.valor = item.valor;
                        city.Add(cityViewModels);
                    }

                    People people = db.People.Where(x => x.id == use.id_people).FirstOrDefault();
                    Status status = db.Status.Where(x => x.id == use.id_status).FirstOrDefault();
                    Profile profiles = db.Profile.Where(x => x.id == use.id_profile).FirstOrDefault();
                    Type_id type_Id = db.Type_id.Where(x => x.id == people.id_type_id).FirstOrDefault();
                    City citys = db.City.Where(x => x.id == people.id_city).FirstOrDefault();
                    Gender genders = db.Gender.Where(x => x.id == people.id_gender).FirstOrDefault();

                    ObjePersonasEntity objCrearPersonas = new ObjePersonasEntity();
                    objCrearPersonas.id = use.id;
                    objCrearPersonas.id_status = status.id;
                    objCrearPersonas.id_profile = profiles.id;
                    objCrearPersonas.id_type_id = type_Id.id;
                    objCrearPersonas.id_city = citys.id;
                    objCrearPersonas.id_gender = genders.id;
                    objCrearPersonas.identifications = people.identifications;
                    objCrearPersonas.names = people.names;
                    objCrearPersonas.surnames = people.surnames;
                    objCrearPersonas.usuario = use.username;
                    objCrearPersonas.email = people.email;
                    objCrearPersonas.phone = people.phone;

                    SedTipoMensaje3 To = new SedTipoMensaje3(profile, type_id, gender, city, objCrearPersonas, 1, "Success", "Datos crear persona");
                    return Ok(To);
                }
                else
                {
                    SedTipoMensaje2 To = new SedTipoMensaje2(2, "Invalid_Access", "Error");
                    return Ok(To);
                }
            }
            catch (Exception ex)
            {
                SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                return Ok(To);
            }
        }

        [HttpPost]
        [Route("POST_UPDATE_PEOPLE")]
        public IHttpActionResult PostUpdatePeople(ObjCrearPersonas objCrearPersonas)
        {
            try
            {
                string IdUser = HttpContext.Current.User.Identity.Name;
                int IdUse = Convert.ToInt32(IdUser);

                Users use = db.Users.FirstOrDefault(x => x.id == IdUse);
                if (use != null && use.id_profile != 3)
                {
                    Users requestUsersExist = db.Users.Where(x => x.id == objCrearPersonas.id).FirstOrDefault();
                    People requestPeopleId = db.People.Where(x => x.id == requestUsersExist.id_people).FirstOrDefault();

                    if (requestUsersExist != null && requestPeopleId != null)
                    {
                        requestPeopleId.id_type_id = objCrearPersonas.id_type_id;
                        requestPeopleId.id_city = objCrearPersonas.id_city;
                        requestPeopleId.identifications = objCrearPersonas.identifications;
                        requestPeopleId.names = objCrearPersonas.names;
                        requestPeopleId.surnames = objCrearPersonas.surnames;
                        requestPeopleId.email = objCrearPersonas.email;
                        requestPeopleId.phone = objCrearPersonas.phone;
                        requestPeopleId.id_gender = objCrearPersonas.id_gender;

                        db.Entry(requestPeopleId).State = EntityState.Modified;
                        try
                        {
                            db.SaveChanges();

                            requestUsersExist.id_profile = objCrearPersonas.id_profile;
                            requestUsersExist.username = objCrearPersonas.usuario;
                            requestUsersExist.password = Cipher.Encrypt(objCrearPersonas.password);

                            db.Entry(requestUsersExist).State = EntityState.Modified;
                            try
                            {
                                db.SaveChanges();
                                SedTipoMensaje2 To = new SedTipoMensaje2(1, "Success", "Se actualizo la persona");
                                return Ok(To);
                            }
                            catch (Exception ex)
                            {
                                SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                                return Ok(To);
                            }
                        }
                        catch (Exception ex)
                        {
                            SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                            return Ok(To);
                        }
                    }
                    else
                    {
                        SedTipoMensaje2 To = new SedTipoMensaje2(4, "Error", "Persona no existe");
                        return Ok(To);
                    }
                }
                else
                {
                    SedTipoMensaje2 To = new SedTipoMensaje2(2, "Invalid_Access", "Error");
                    return Ok(To);
                }
            }
            catch (Exception ex)
            {
                SedTipoMensaje2 To = new SedTipoMensaje2(3, "Exception", ex.Message);
                return Ok(To);
            }
        }
    }
}