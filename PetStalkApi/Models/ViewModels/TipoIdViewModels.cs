﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetStalkApi.Models.ViewModels
{
    public class TipoIdViewModels
    {
        public int id { get; set; }
        public string valor { get; set; }
    }
}