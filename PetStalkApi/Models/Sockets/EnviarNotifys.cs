﻿using System;
using System.Configuration;
using System.IO;
using System.Net;


namespace PetStalkApi.Models.Sockets
{
    public class EnviarNotifys
    {
        public static bool SendNotifays(string type, string payload, string url, string asunto, string ex, string estado, string descripcion, int id_usuario, int id_estado, DateTime fecha_sistema, int id_general)
        {
            bool reult = true;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["URL_NOTI"].ToString());
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"type\":\"" + type + "\"," +
                                "\"asunto\":\"" + asunto + "\"," +
                                "\"descripcion\":\"" + descripcion + "\"," +
                                "\"id_estado\":" + id_estado + "," +
                                "\"id_usuario\":" + id_usuario + "," +
                                "\"ex\":\" " + ex + "\"," +
                                "\"url\":\" " + url + "\"," +
                                "\"estado\":\" " + estado + "\"," +
                                "\"id_general\":" + id_general + "," +
                                "\"Payload\":\" " + payload + "\"}";
                streamWriter.Write(json);
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                httpResponse.Close();
                streamReader.Close();

                return reult;
            }
        }
    }
}