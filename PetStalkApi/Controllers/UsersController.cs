﻿using PetStalkApi.Models;
using PetStalkApi.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PetStalkApi.Controllers
{
    [Authorize]
    [RoutePrefix("API/USERS")]
    public class UsersController : ApiController
    {
        private FormsIntelligentEntities db = new FormsIntelligentEntities();
        public struct SedTipoMensaje
        {
            public List<ProfileViewModels> ListProfile;
            public List<TipoIdViewModels> ListTipoId;
            public List<GenderViewModels> ListGender;
            public List<CityViewModels> ListCity;
            public string Status;
            public SedTipoMensaje(List<ProfileViewModels> listProfile, List<TipoIdViewModels> lisTipoId, List<GenderViewModels> listGender, List<CityViewModels> listCity, string status)
            {
                ListProfile = listProfile;
                ListTipoId = lisTipoId;
                ListGender = listGender;
                ListCity = listCity;
                Status = status;
            }
        }

        [HttpGet]
        [Route("GET_USERS")]
        public IHttpActionResult GetUsers()
        {            
            try
            {
                string IdUser = HttpContext.Current.User.Identity.Name;
                int IdUse = Convert.ToInt32(IdUser);

                Users use = db.Users.FirstOrDefault(x => x.id == IdUse);
                if (use != null)
                {
                    People peoples = db.People.FirstOrDefault(x => x.id == use.id_people);
                    PeopleViewModels People = new PeopleViewModels();

                    People.id_users = use.id;
                    People.id_perfil = use.id_profile;
                    People.id_status = use.id_status;
                    People.nombre = peoples.names + " " + peoples.surnames;
                    People.username = use.username;
                    People.status = "Success";

                    return Ok(People);
                }
                else
                {
                    PeopleViewModels People = new PeopleViewModels();
                    People.status = "Error";

                    return Ok(People);
                }
            }
            catch (Exception ex)
            {
                PeopleViewModels People = new PeopleViewModels();
                People.status = ex.Message;

                return Ok(People);
            }            
        }
    }
}