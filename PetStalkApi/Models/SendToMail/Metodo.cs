﻿using PetStalkApi.Controllers;
using PetStalkApi.Models.Sockets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace PetStalkApi.Models.SendToMail
{
    public class Metodo : ApiController
    {
        SendMail Send = new SendMail();

        public IHttpActionResult SendToMail()
        {
            bool res = Send.SendCorreo("", "", "", "Solicitud de Compra paquete", "Su transacción esta en proceso", "Solicitud de Transacción", "", "", "", "");
            return Ok(res);
        }
    }
}